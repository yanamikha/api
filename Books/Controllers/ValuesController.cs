﻿using Books.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Books.Controllers
{
    public class ValuesController : ApiController
    {
        public List<Book> books;
        public ValuesController()
        {
            books = new List<Book>();
            books.Add(new Book(1, "hh", "hh", "hh", 1998));
            books.Add(new Book(2, "hh", "hh", "hh", 1998));
            books.Add(new Book(3, "hh", "hh", "hh", 1998));
        }
     

        public IEnumerable<Book> GetBooks()
        {
            return books;
        }

        public Book GetBook(int id)
        {
            foreach (var b in books)
            {
                if (b.BookId == id)
                    return b;
            }
            return null;
        }

        [HttpPost]
        public void CreateBook([FromBody]Book book)
        {
            books.Add(book);
        }


        public void DeleteBook(int id)
        {
            foreach (var b in books)
            {
                if (b.BookId == id)
                {
                    books.Remove(b);
                }
            }
        }
    }
}
